import React ,{Component} from 'react';
import Item from './item/item'

class Items extends Component{

    state={
        items:[
            {id:1,todo:"programming",date:'01-09-19'},
            {id:2,todo:"programming",date:'01-09-19'} ,
            {id:3,todo:"programming",date:'01-09-19'}    
    
        ]
    }

    addItems = ()=>{
        var todo = document.getElementById('input').value
        
        var items = this.state.items;
        var item = {id:items.length+1,todo:todo,date:''}
        items.push(item);

        this.setState({
            items
        })
    }

    render(){
        return (
            <div>
                <div className = 'items'>
                    {this.state.items.map( e => <Item key = {e.id} id = {e.id} todo ={e.todo} date={e.date}/> )}

                </div>
                <input type = 'text' id = 'input'/>
                <button onClick = {this.addItems}>add</button>
            </div>
        )
    }
}

export default Items;