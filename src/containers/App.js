import React, { Component } from 'react';
import './App.css';

import Todo from './todo'
class App extends Component {
  render() {
    return (
      
      <div className='app'>
        <Todo/>
        <h1>hello this is app component</h1>
      </div>
    );
  }
}

export default App;
